function a = ValidaVal(val, min, zero)

%ValidarVal
%  val    Valor a ser verificado
%  min    Indica se a função deve impor ao valor um máximo
%  zero   Indica se a função deve garantir que o valor não é zero
%
%------------------------------------------------------------------------
%  Bruno Sabença <a21180264@alunos.isec.pt>
%------------------------------------------------------------------------

a = 0;
if (~isnan(val) && ~isempty(val) && isnumeric(val))
    if (~isempty(zero))
        if (val == 0)
            a = 1;
        end
    end
    if (~isempty(min))
        if (val < str2num(min))
            a = 1;
        end
    end
    return;
else
    a = 1;
    return;
end
