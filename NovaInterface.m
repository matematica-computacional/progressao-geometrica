function varargout = NovaInterface(varargin)
% NOVAINTERFACE MATLAB code for NovaInterface.fig
%      NOVAINTERFACE, by itself, creates a new NOVAINTERFACE or raises the existing
%      singleton*.
%
%      H = NOVAINTERFACE returns the handle to a new NOVAINTERFACE or the handle to
%      the existing singleton*.
%
%      NOVAINTERFACE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in NOVAINTERFACE.M with the given input arguments.
%
%      NOVAINTERFACE('Property','Value',...) creates a new NOVAINTERFACE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before NovaInterface_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to NovaInterface_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help NovaInterface

% Last Modified by GUIDE v2.5 12-Mar-2017 11:27:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @NovaInterface_OpeningFcn, ...
                   'gui_OutputFcn',  @NovaInterface_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before NovaInterface is made visible.
function NovaInterface_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to NovaInterface (see VARARGIN)

% Choose default command line output for NovaInterface
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes NovaInterface wait for user response (see UIRESUME)
% uiwait(handles.figProgressaoGeometrica);


% --- Outputs from this function are returned to the command line.
function varargout = NovaInterface_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edtPrimeiroElemento_Callback(hObject, eventdata, handles)
% hObject    handle to edtPrimeiroElemento (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtPrimeiroElemento as text
%        str2double(get(hObject,'String')) returns contents of edtPrimeiroElemento as a double


% --- Executes during object creation, after setting all properties.
function edtPrimeiroElemento_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtPrimeiroElemento (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edtRazao_Callback(hObject, eventdata, handles)
% hObject    handle to edtRazao (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtRazao as text
%        str2double(get(hObject,'String')) returns contents of edtRazao as a double
try
    Razao = str2double(get(hObject,'String'));
    assert(~isnan(Razao))
catch
    warndlg('A razão deve ser um valor numérico.');
    set(hObject,'String','2');
end

% --- Executes during object creation, after setting all properties.
function edtRazao_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtRazao (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edtNumeroElementos_Callback(hObject, eventdata, handles)
% hObject    handle to edtNumeroElementos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtNumeroElementos as text
%        str2double(get(hObject,'String')) returns contents of edtNumeroElementos as a double
try
    NumeroElementos = str2double(get(hObject,'String'));
    assert(~isnan(NumeroElementos) && NumeroElementos >= 2)
catch
    warndlg('Número de elementos deve ser igual ou superior a 2.');
    set(hObject,'String','2');
end

% --- Executes during object creation, after setting all properties.
function edtNumeroElementos_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtNumeroElementos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btnCalcular.
function btnCalcular_Callback(hObject, eventdata, handles)
% hObject    handle to btnCalcular (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
a_1 = str2double(get(handles.edtPrimeiroElemento,'String'));
r   = str2double(get(handles.edtRazao,'String'));
n   = str2double(get(handles.edtNumeroElementos,'String'));

versao_algoritmo = get(handles.popVersao,'Value');

invalidos = [0 0 0];
invalidos(1) = ValidaVal(a_1,'', '1');
invalidos(2) = ValidaVal(r,  '', '1');
invalidos(3) = ValidaVal(n,  '2','1');

if (nnz(invalidos) ~= 0)
    strInvalidos = 'Parâmetros inválidos:';
    if (invalidos(1) ~= 0)
        strPrimeiroElemento = 'Primeiro elemento não deve ser zero.';
        strInvalidos = char(strInvalidos, strPrimeiroElemento);
    end
    if (invalidos(2) ~= 0)
        strRazao = 'Razão não deve ser zero.';
        strInvalidos = char(strInvalidos, strRazao);
    end
    if (invalidos(3) ~= 0)
        strNumeroElementos = 'Número de elementos deve ser no mínimo 2.';
        strInvalidos = char(strInvalidos, strNumeroElementos);
    end
    set(handles.txtResultado, 'String', strInvalidos);
else
    switch (versao_algoritmo)
        case 1
            a = ProgressaoGeometrica_v1(a_1,r,n);
        case 2
            a = ProgressaoGeometrica_v2(a_1,r,n);
        case 3
            a = ProgressaoGeometrica_v3(a_1,r,n);
        case 4
            a = ProgressaoGeometrica_v4(a_1,r,n);
    end
    set(handles.txtResultado, 'String', a);
end


% --- Executes on selection change in popVersao.
function popVersao_Callback(hObject, eventdata, handles)
% hObject    handle to popVersao (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popVersao contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popVersao



% --- Executes during object creation, after setting all properties.
function popVersao_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popVersao (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btnTemporizar.
function btnTemporizar_Callback(hObject, eventdata, handles)
% hObject    handle to btnTemporizar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
a_1 = str2double(get(handles.edtPrimeiroElemento,'String'));
r   = str2double(get(handles.edtRazao,'String'));
n   = str2double(get(handles.edtNumeroElementos,'String'));

invalidos = [0 0 0];
invalidos(1) = ValidaVal(a_1,'', '1');
invalidos(2) = ValidaVal(r,  '', '1');
invalidos(3) = ValidaVal(n,  '2','1');

if (nnz(invalidos) ~= 0)
    strInvalidos = 'Parâmetros inválidos:';
    if (invalidos(1) ~= 0)
        strPrimeiroElemento = 'Primeiro elemento não deve ser zero.';
        strInvalidos = char(strInvalidos, strPrimeiroElemento);
    end
    if (invalidos(2) ~= 0)
        strRazao = 'Razão não deve ser zero.';
        strInvalidos = char(strInvalidos, strRazao);
    end
    if (invalidos(3) ~= 0)
        strNumeroElementos = 'Número de elementos deve ser no mínimo 2.';
        strInvalidos = char(strInvalidos, strNumeroElementos);
    end
    set(handles.txtResultado, 'String', strInvalidos);
else
    tempos = zeros(4);
    for i = 1:4
        switch (i)
            case 1
                f = @() ProgressaoGeometrica_v1(a_1,r,n);
            case 2
                f = @() ProgressaoGeometrica_v2(a_1,r,n);
            case 3
                f = @() ProgressaoGeometrica_v3(a_1,r,n);
            case 4
                f = @() ProgressaoGeometrica_v4(a_1,r,n);
        end
        tempos(i) = timeit(f);
        strTempos{i} = ['Versão ' num2str(i) ':  ' num2str(tempos(i))];
    end
    strResultado = char(strTempos{1},strTempos{2},strTempos{3},strTempos{4});
    set(handles.txtResultado, 'String', strResultado);
end
