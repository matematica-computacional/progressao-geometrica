function a = ProgressaoGeometrica_v3(a_1,r,n)
%ProgressaoGeometrica_v3 Gerar uma Progressão Geométrica (PG)
%   a = ProgressaoGeometrica(a_1,r,n)
%   a(n) = a(1) * r^(i-1)
%
%INPUT:
%   a1 - primeiro elemento da progressão geométrica
%   r - razão da progressão geométrica
%   n - número de elementos da progressão geométrica
%
%OUTPUT:
%   a - vector com os elementos da progressão geométrica
%   a(i) = a(1) * r^(i-1) , i=2,...,n
%
%-------------------------------------------------------------
%	Versão original para Progressões Aritméticas:
%		Arménio Correia <armenioc@isec.pt>
%	Adaptação para Progressões Geométricas:
%		Bruno Sabença <a21180264@alunos.isec.pt>
%-------------------------------------------------------------

a(1) = a_1;
i = 2;
while(1)
    a(i) = a(1) * r .^ (i-1);
    if (i >= n)
        break;
    end
    i = i + 1;
end

