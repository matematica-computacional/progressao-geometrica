function S = SomaPGeometrica(a)
%SomaPGeometrica Soma dos elementos de uma Progressão Geométrica (PG)
%   a = SomaPGeometrica(a)
%   S(n) = a(1) * (r^n-1) / (r-1)
%
%INPUT:
%   a - vector com os elementos da PG
%
%OUTPUT:
%   S - Soma dos elementos da Progressão Geométrica
%   S(n) = a(1) * (r^n-1) / (r-1)
%
%-------------------------------------------------------------
%	Versão original para Progressões Aritméticas:
%		Arménio Correia <armenioc@isec.pt>
%	Adaptado para Progressões Geométricas:
%		Bruno Sabença <a21180264@alunos.isec.pt>
%-------------------------------------------------------------

n = length(a);		% número de elementos da PG
r = a(2) / a(1);	% determinação da razão da PG

S = a(1) * (r.^n-1) / (r-1);
